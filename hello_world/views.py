from hello_world import app
from hello_world.formater import get_formatted
from hello_world.formater import SUPPORTED, PLAIN
from flask import request

moje_imie = "Joanna"
msg = "Hello World!"


@app.route('/')
def index():
    output = request.args.get('output')
    name = request.args.get('name')
    if not output:
        output = PLAIN
    if not name:
        imie = moje_imie
    else:
        imie = name
    return get_formatted(msg, imie, output.lower())


@app.route('/outputs')
def supported_output():
    return ", ".join(SUPPORTED)
