## Continuous Deployment procces description

![](continuous_deployment.png)

### Installation

We need a virtual environment to separate the Python environment and Docker.

##### Ubuntu:

Installation python virtualenv i virtualenvwrapper:

```
$ sudo pip install virtualenv
$ sudo pip install virtualenvwrapper
```

Install docker: https://docs.docker.com/engine/install/ubuntu/


##### Centos:

Installation python virtualenv i virtualenvwrapper:

```
$ yum install -y python-pip
$ pip install -U pip
$ pip install virtualenv
$ pip install virtualenvwrapper
```

Install docker: https://docs.docker.com/engine/install/centos/


### Prepering environment:

```
virtualenv  wsb-simple-flask-app
source ./wsb-simple-flask-app/bin/activate
```

```
$ pip install -r requirements.txt
$ pip install -r test_requirements.txt
```

Deactivating the environment:

```
deactivate
```

### Running application:
Run application more efficiently with makefile.
The make utility requires a file, _Makefile_ (or _makefile_), which defines set of tasks to be executed.

- **Run Flash App**

  `$ make run`
  
  You can acces on 127.0.0.1:5000

- **Runing tests**

   ```
   $ make lint
   $ make test
   $ make test_cov
   $ make test_xunit
   $ make test_smoke
   ```


- **Docker**

    ```
    $ make docker_build
    $ make docker_run
    $ make docker_stop
    $ make docker_push
    ```     
   

* **Installation Jenkins**

   Jenkins provides hundreds of plugins to support building, deploying and automating any project.
 
   ```
   git clone https://github.com/JDmocho/se_teaching_jenkins
   $ make build_jenkins
   $ make run_jenkins
   ```
   You can access Jankins on 127.0.0.1:8080
   
   Password you can find here:
   
   `$ cat jenkins/secrets/initialAdminPassword`


- **Travis Integration**:

    The simplest way to test and deploy our project is to use Travis CI.
    Easily sync our project with Travis CI and you'll be testing your code in minutes.
    More info in _.travis.yml_ file


- **Deploy on Heroku**

    Heroku is a cloud platform that lets you build, deliver, monitor and scale apps. The fastest way to go from idea to URL, bypassing all those infrastructure headaches.
    You can access:  https://shrouded-lowlands-34261.herokuapp.com/
    
- **Push to docker repository**    
    
    After success build and test push image to Docker Hub: https://hub.docker.com/repository/docker/jdmocho/hello-world-printer